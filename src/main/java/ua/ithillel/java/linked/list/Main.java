package ua.ithillel.java.linked.list;

import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //  List<String> list = new LinkedList<>();
        MyLinkedList<String> myLinkedList = new MyLinkedList<>();
        MyLinkedList<String> myLinkedList2 = new MyLinkedList<>();
        MyLinkedList<String> myLinkedList3 = new MyLinkedList<>();
        myLinkedList.add("0");
        myLinkedList.add("1");
        myLinkedList.add("2");
        myLinkedList.add("3");
        myLinkedList.add("4");
        myLinkedList.add("5");

        myLinkedList2.add("10");
        myLinkedList2.add("11");
        myLinkedList2.add("12");
        myLinkedList2.add("13");

//        list.add("0");
//        list.add("1");
//        list.add("2");
//        list.add("3");
//        list.add("4");
//        list.add("5");


        for (String s : myLinkedList) {
            System.out.println(s);
        }
        System.out.println("size = " + myLinkedList.size());
        System.out.println("*****");

        myLinkedList.addAll(3, myLinkedList2);

        for (String s : myLinkedList) {
            System.out.println(s);
        }
        System.out.println("size = " + myLinkedList.size());

//        for (String s : list) {
//            System.out.println(s);
//        }
//        System.out.println("size = " + list.size());
//        System.out.println("*****");
//
//        list.add(6, "qwe");
//
//        for (String s : list) {
//            System.out.println(s);
//        }
//        System.out.println("size = " + list.size());
    }
}
