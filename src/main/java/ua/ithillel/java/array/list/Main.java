package ua.ithillel.java.array.list;

public class Main {
    public static void main(String[] args) {
        MyArrayList<String> myArrayList = new MyArrayList<>();

        myArrayList.add("0");
        myArrayList.add("1");
        myArrayList.add("2");
        myArrayList.add("3");
        myArrayList.add("3");
        myArrayList.add("3");
        myArrayList.add("4");
        myArrayList.add("5");
        myArrayList.add("6");

        MyArrayList<String> myArrayList2 = new MyArrayList<>();

        myArrayList2.add("3");
        myArrayList2.add("5");
        myArrayList2.add("6");
        myArrayList2.add("10");
        myArrayList2.add("11");

        for (String s : myArrayList) {
            System.out.println(s);
        }
        System.out.println("size = " + myArrayList.size());

        myArrayList.retainAll(myArrayList2);

        System.out.println("**********");
        System.out.println("size = " + myArrayList.size());
        for (String s : myArrayList) {
            System.out.println(s);
        }
    }
}
