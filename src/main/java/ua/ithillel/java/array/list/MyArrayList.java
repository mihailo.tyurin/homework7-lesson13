package ua.ithillel.java.array.list;

import java.util.*;


public class MyArrayList<T> implements List<T> {
    private static final int INITIAL_SIZE = 8;
    private static final int SCALE_FACTOR = 2;

    private Object[] arr = new Object[INITIAL_SIZE];
    private int size = 0;

    @Override
    public boolean add(T elem) {
        if (arr.length == size) expand();
        arr[size++] = elem;
        return true;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public T get(int index) {
        checkIndex(index);
        return (T) arr[index];
    }

    @Override
    public T set(int index, T element) {
        checkIndex(index);
        T previousElem = (T) arr[index];
        arr[index] = element;
        return previousElem;
    }

    @Override
    public Iterator<T> iterator() {
        return new MyArrayListIterator();
    }

    @Override
    public boolean remove(Object o) {
        int i = indexOf(o);
        if (i >= 0) {
            while (i < size) {
                arr[i] = arr[i + 1];
                i++;
            }
            arr = Arrays.copyOf(arr, arr.length - 1);
            size--;
            return true;
        } else return false;
    }

    @Override
    public T remove(int index) {
        checkIndex(index);
        T removedElem = (T) arr[index];
        for (int i = 0; i < size - 1; i++) {
            if (i < index) {
                continue;
            } else {
                arr[i] = arr[i + 1];
            }
        }
        arr = Arrays.copyOf(arr, arr.length - 1);
        size--;
        return removedElem;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object elem : c) {
            if (!contains(elem)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T elem : c) {
            add(elem);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        for (T elem : c) {
            add(index, elem);
            index++;
        }
        return c.size() > 0;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        if (c.isEmpty()) {
            return false;
        } else {
            for (Object elem : c) {
                while (remove(elem)) ;
            }
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Object[] tmp = new Object[arr.length];
        int j = 0;
        if (c.isEmpty()) {
            return false;
        } else {
            for (Object elem : c) {
                for (int i = 0; i < size; i++) {
                    if (elem.equals(arr[i])) {
                        tmp[j] = arr[i];
                        j++;
                    }
                }
            }
            size = j;
            arr = Arrays.copyOf(tmp, size);
        }
        return true;
    }

    @Override
    public void clear() {
        arr = new Object[INITIAL_SIZE];
        size = 0;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < size; i++) {
            if (arr[i].equals(o)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        for (int i = size - 1; i >= 0; i--) {
            if (arr[i].equals(o)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        checkIndex(fromIndex);
        checkIndex(toIndex);
        List<T> newList = new MyArrayList<>();
        for (int i = fromIndex; i < toIndex; i++) {
            newList.add(get(i));
        }
        return newList;
    }

    @Override
    public void add(int index, T element) {
        if (index == size) {
            add(element);
        } else {
            checkIndex(index);
            size++;
            for (int i = size - 1; i >= index; i--) {
                if (i == index) {
                    arr[i] = element;
                } else {
                    arr[i] = arr[i - 1];
                }
            }
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(arr, size);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        T1[] arrCopy = Arrays.copyOf(a, size);
        System.arraycopy(arr, 0, arrCopy, 0, size);
        return arrCopy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyArrayList<?> that = (MyArrayList<?>) o;
        return Arrays.equals(arr, that.arr);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(arr);
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException(index);
        }
    }

    private void expand() {
        int newLength = arr.length * SCALE_FACTOR;
        Object[] tmp = new Object[newLength];
        for (int i = 0; i < arr.length; i++) {
            tmp[i] = arr[i];
        }
        arr = tmp;
    }

    private class MyArrayListIterator implements Iterator<T> {
        int i = 0;

        @Override
        public boolean hasNext() {
            return i < size;
        }

        @Override
        public T next() {
            T elem = (T) arr[i];
            i++;
            return elem;
        }
    }
}
