package ua.ithillel.java.linked.list;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ua.ithillel.java.array.list.MyArrayList;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MyLinkedListTest {

    private static MyArrayList<String> LIST;
    private static MyArrayList<String> LIST2;
    private static MyArrayList<String> LIST3;


    @BeforeEach
    void prepareData() {
        LIST = new MyArrayList<>();
        LIST2 = new MyArrayList<>();
        LIST3 = new MyArrayList<>();

        LIST.add("zero");
        LIST.add("one");
        LIST.add("two");
        LIST.add("three");

        LIST2.add("ten");
        LIST2.add("eleven");

        LIST3.add("zero");
        LIST3.add("two");

    }

    @Test
    void shouldGetSizeList() {
        assertEquals(4, LIST.size());
        LIST.add("Hello");
        LIST.add("world");
        assertEquals(6, LIST.size());
    }

    @Test
    void shouldCheckListIsEmpty() {
        assertFalse(LIST.isEmpty());

        LIST.clear();

        assertTrue(LIST.isEmpty());
    }

    @Test
    void shouldCheckElemOnContains() {
        assertTrue(LIST.contains("zero"));
        assertFalse(LIST.contains("hello"));

        LIST.add("hello");

        assertTrue(LIST.contains("hello"));
    }

    @Test
    void iterator() {
    }

    @Test
    void reverseIterator() {
    }

    @Test
    void shouldChangeListToArray() {
        Object[] strArr = LIST.toArray();
        assertEquals(4, strArr.length);
        assertEquals("one", strArr[1]);
    }

    @Test
    void testToArray() {
    }

    @Test
    void shouldAddElem() {
        assertEquals(4, LIST.size());

        LIST.add("hello");
        LIST.add("World");

        assertEquals(6, LIST.size());
        assertEquals("hello", LIST.get(4));
        assertEquals("World", LIST.get(5));
    }

    @Test
    void shouldRemoveElementByValue() {
        assertEquals(2, LIST3.size());

        LIST3.remove("zero");
        assertEquals(1, LIST3.size());

        LIST3.remove("two");
        assertTrue(LIST3.isEmpty());

    }

    @Test
    void shouldCheckListOnContainsAll() {
        assertFalse(LIST.containsAll(LIST2));
        assertTrue(LIST.containsAll(LIST3));
    }

    @Test
    void shouldAddAllElem() {
        assertEquals(4, LIST.size());

        LIST.addAll(LIST2);
        assertEquals(6, LIST.size());
        assertEquals("ten", LIST.get(4));
        assertEquals("eleven", LIST.get(5));

        LIST.addAll(LIST3);
        assertEquals(8, LIST.size());
        assertEquals("zero", LIST.get(6));
        assertEquals("two", LIST.get(7));
    }

    @Test
    void shouldAddAllElemInSpecificPlace() {
        assertEquals(4, LIST.size());

        LIST.addAll(2, LIST2);
        assertEquals(6, LIST.size());
        assertEquals("ten", LIST.get(2));
        assertEquals("eleven", LIST.get(3));
        assertEquals("two", LIST.get(4));
    }

    @Test
    void shouldRemoveAllElem() {
        assertEquals(4, LIST.size());

        LIST.removeAll(LIST3);

        assertEquals(2, LIST.size());
        assertEquals("one", LIST.get(0));
        assertEquals("three", LIST.get(1));
    }

    @Test
    void shouldRetainAllElem() {
        assertEquals(4, LIST.size());

        LIST.retainAll(LIST3);

        assertEquals(2, LIST.size());
        assertEquals("zero", LIST.get(0));
        assertEquals("two", LIST.get(1));
    }

    @Test
    void shouldClearList() {
        assertEquals(4, LIST.size());

        LIST.clear();
        assertTrue(LIST.isEmpty());
    }

    @Test
    void shouldGetElemByIndex() {
        assertEquals("two", LIST.get(2));
        assertEquals("three", LIST.get(3));
    }

    @Test
    void shouldChangeElemInList() {
        assertEquals("two", LIST.get(2));
        String prevValue = LIST.set(2, "hello");
        assertEquals("two", prevValue);
        assertEquals("hello", LIST.get(2));
    }

    @Test
    void shouldAddElemInSpecificPlace() {
        assertEquals(4, LIST.size());
        assertEquals("two", LIST.get(2));

        LIST.add(2, "hello");

        assertEquals(5, LIST.size());
        assertEquals("hello", LIST.get(2));
        assertEquals("two", LIST.get(3));
    }

    @Test
    void shouldRemoveFromSpecificPlace() {
        assertEquals(4, LIST.size());
        assertEquals("two", LIST.get(2));

        LIST.remove(2);
        assertEquals(3, LIST.size());
        assertEquals("three", LIST.get(2));

    }

    @Test
    void shouldReturnIndexOf() {
        LIST.addAll(LIST3);
        LIST.addAll(LIST2);

        int index = LIST.indexOf("zero");
        assertEquals(0, index);

        index = LIST.indexOf("ten");
        assertEquals(6, index);

        index = LIST.indexOf("Hello");
        assertEquals(-1, index);
    }

    @Test
    void shouldReturnlastIndexOf() {
        LIST.addAll(LIST3);
        LIST.addAll(LIST2);

        int index = LIST.lastIndexOf("zero");
        assertEquals(4, index);

        index = LIST.lastIndexOf("two");
        assertEquals(5, index);

        index = LIST.lastIndexOf("Hello");
        assertEquals(-1, index);
    }

    @Test
    void listIterator() {
    }

    @Test
    void testListIterator() {
    }

    @Test
    void shouldMakeSubList() {
        LIST.addAll(LIST3);
        LIST.addAll(LIST2);

        MyArrayList<String> LIST4 = (MyArrayList<String>) LIST.subList(2, 5);
        assertEquals(3, LIST4.size());
        assertEquals("two", LIST4.get(0));
        assertEquals("three", LIST4.get(1));
        assertEquals("zero", LIST4.get(2));
    }
}