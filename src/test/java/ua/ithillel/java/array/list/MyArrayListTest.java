package ua.ithillel.java.array.list;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class MyArrayListTest {

    private static MyArrayList<String> LIST;
    private static MyArrayList<String> LIST2;
    private static MyArrayList<String> LIST3;


    @BeforeEach
    void prepareData() {
        LIST = new MyArrayList<>();
        LIST2 = new MyArrayList<>();
        LIST3 = new MyArrayList<>();

        LIST.add("zero");
        LIST.add("one");
        LIST.add("two");
        LIST.add("three");

        LIST2.add("ten");
        LIST2.add("eleven");

        LIST3.add("zero");
        LIST3.add("two");

    }

    @Test
    void shouldAddElem() {
        int sizeList = LIST.size();

        assertEquals(4, sizeList);

        LIST.add("hello world!");

        sizeList = LIST.size();

        assertEquals(5, sizeList);
        assertEquals("hello world!", LIST.get(4));
    }

    @Test
    void shouldGetSize() {
        int sizeList = LIST.size();
        assertEquals(4, sizeList);
    }

    @Test
    void shouldGetValue() {
        String value = LIST.get(1);
        assertEquals("one", value);
    }

    @Test
    void setTest() {
        assertEquals("two", LIST.get(2));

        String value = LIST.set(2, "Hello!");

        assertEquals("two", value);
        assertEquals("Hello!", LIST.get(2));

    }

    @Test
    void iterator() {
    }

    @Test
    void shouldRemoveElemByValue() {
        assertEquals(4, LIST.size());
        boolean isRemoved = LIST.remove("one");
        assertTrue(isRemoved);
        assertEquals(3, LIST.size());

        isRemoved = LIST.remove("hello");
        assertFalse(isRemoved);
        assertEquals(3, LIST.size());
    }

    @Test
    void shouldRemoveElemByIndex() {
        assertEquals(4, LIST.size());

        String removedElement = LIST.remove(2);

        assertEquals("two", removedElement);
        assertEquals(3, LIST.size());
    }

    @Test
    void shouldCheckElemOnContains() {
        boolean isContain = LIST.contains("one");
        assertEquals(true, isContain);

        isContain = LIST.contains("hello");
        assertEquals(false, isContain);
    }

    @Test
    void shouldCheckElemOnContainsAll() {
        boolean isContain = LIST.containsAll(LIST3);
        assertTrue(isContain);

        isContain = LIST.containsAll(LIST2);
        assertFalse(isContain);
    }

    @Test
    void shouldAddAll() {
        assertEquals(4, LIST.size());

        LIST.addAll(LIST2);

        assertEquals(6, LIST.size());
        assertEquals("eleven", LIST.get(5));
    }

    @Test
    void shouldAddAllInSpecificPlace() {
        assertEquals(4, LIST.size());
        assertEquals("two", LIST.get(2));

        LIST.addAll(2, LIST2);

        assertEquals(6, LIST.size());
        assertEquals("ten", LIST.get(2));
        assertEquals("eleven", LIST.get(3));
        assertEquals("two", LIST.get(4));
    }

    @Test
    void shouldRemoveAllElem() {
        assertEquals(4, LIST.size());

        LIST.removeAll(LIST3);

        assertEquals(2, LIST.size());
        assertEquals("one", LIST.get(0));
        assertEquals("three", LIST.get(1));
    }

    @Test
    void shouldRetainAllElem() {
        assertEquals(4, LIST.size());

        LIST.retainAll(LIST3);

        assertEquals(2, LIST.size());
        assertEquals("zero", LIST.get(0));
        assertEquals("two", LIST.get(1));
    }

    @Test
    void shouldClearList() {
        assertEquals(4, LIST.size());

        LIST.clear();

        assertEquals(0, LIST.size());
    }

    @Test
    void shouldReturnIndexOf() {
        LIST.addAll(LIST3);
        LIST.addAll(LIST2);

        int index = LIST.indexOf("zero");
        assertEquals(0, index);

        index = LIST.indexOf("ten");
        assertEquals(6, index);

        index = LIST.indexOf("Hello");
        assertEquals(-1, index);
    }

    @Test
    void shouldReturnLastIndexOf() {
        LIST.addAll(LIST3);
        LIST.addAll(LIST2);

        int index = LIST.lastIndexOf("zero");
        assertEquals(4, index);

        index = LIST.lastIndexOf("two");
        assertEquals(5, index);

        index = LIST.lastIndexOf("Hello");
        assertEquals(-1, index);
    }

    @Test
    void shouldMakeSubList() {
        LIST.addAll(LIST3);
        LIST.addAll(LIST2);

        MyArrayList<String> LIST4 = (MyArrayList<String>) LIST.subList(2, 5);
        assertEquals(3, LIST4.size());
        assertEquals("two", LIST4.get(0));
        assertEquals("three", LIST4.get(1));
        assertEquals("zero", LIST4.get(2));
    }

    @Test
    void shouldAddElemOnSpecificPlace() {
        assertEquals(4, LIST.size());
        assertEquals("two", LIST.get(2));

        LIST.add(2, "hello");

        assertEquals(5, LIST.size());
        assertEquals("hello", LIST.get(2));
        assertEquals("two", LIST.get(3));
    }

    @Test
    void shouldCheckIsEmptyList() {
        assertFalse(LIST.isEmpty());

        LIST.clear();

        assertTrue(LIST.isEmpty());
    }

    @Test
    void toArray() {
        Object[] strArr = LIST.toArray();
        assertEquals(4, strArr.length);
        assertEquals("zero", strArr[0]);
    }

    @Test
    void testToArray() {
    }
}